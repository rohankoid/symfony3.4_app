<?php

namespace AppBundle\Entity;

use AppBundle\Repository\ContactRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Country;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false, options={"default": " "})
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="street_and_number", type="string", length=255, nullable=true)
     */
    private $streetAndNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var int
     *
     * @ORM\Column(name="zip_code", type="integer", length=9, nullable=false)
     */
    private $zipCode = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=20)
     */
    private $phoneNumber;

    /**
     * @var int
     *
     * @ORM\Column(name="birth_day", type="integer", nullable=true, options={"default": 1})
     */
    private $birthDay;

    /**
     * @var string
     *
     * @ORM\Column(name="birth_month", type="string", length=15, nullable=true, options={"default": "JANUARY"})
     */
    private $birthMonth;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255)
     */
    private $emailAddress;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set streetAndNumber
     *
     * @param string $streetAndNumber
     *
     * @return Contact
     */
    public function setStreetAndNumber($streetAndNumber)
    {
        $this->streetAndNumber = $streetAndNumber;

        return $this;
    }

    /**
     * Get streetAndNumber
     *
     * @return string
     */
    public function getStreetAndNumber()
    {
        return $this->streetAndNumber;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Contact
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Contact
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Contact
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set birthDay
     *
     * @param integer $birthDay
     *
     * @return Contact
     */
    public function setBirthDay($birthDay)
    {
        $this->birthDay = $birthDay;

        return $this;
    }

    /**
     * Get birthDay
     *
     * @return int
     */
    public function getBirthDay()
    {
        return $this->birthDay;
    }

    /**
     * Set birthMonth
     *
     * @param string $birthMonth
     *
     * @return Contact
     */
    public function setBirthMonth($birthMonth)
    {
        $this->birthMonth = $birthMonth;

        return $this;
    }

    /**
     * Get birthMonth
     *
     * @return string
     */
    public function getBirthMonth()
    {
        return $this->birthMonth;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Contact
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @return int
     */
    public function getZipCode(): int
    {
        return $this->zipCode;
    }

    /**
     * @param int $zipCode
     */
    public function setZipCode(int $zipCode)
    {
        $this->zipCode = $zipCode;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraints('firstName',
            array(
                new NotBlank(array('message' => 'First name cannot be blank')),
                new Length([
                    'min' => 2,
                    'max' => 50,
                    'minMessage' => 'Your first name must be at least {{ limit }} characters long',
                    'maxMessage' => 'Your first name cannot be longer than {{ limit }} characters',
                ])
            )
        );

        $metadata->addPropertyConstraints('lastName',
            array(
                new NotBlank(array('message' => 'Last name cannot be blank')),
                new Length([
                    'min' => 2,
                    'max' => 50,
                    'minMessage' => 'Your last name must be at least {{ limit }} characters long',
                    'maxMessage' => 'Your last name cannot be longer than {{ limit }} characters',
                ])
            )
        );
        $metadata->addPropertyConstraints('emailAddress',
            array(
                new Email(array('message' => 'The email "{{ value }}" is not a valid email.',
                    'checkMX' => true,)))
        );
        $metadata->addPropertyConstraint('birthMonth',new Choice(['choices' => ContactRepository::MONTHS]));
        $metadata->addPropertyConstraints('birthDay',
            array(
                new Range([
                    'min' => 1,
                    'max' => 31,
                    'minMessage' => 'Please enter a valid date',
                    'maxMessage' => 'Please enter a valid date',
                ])
            )
        );

    }

}

