<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ContactControllerTest extends WebTestCase
{

    public function testFalseValidationScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /contact/");
        $crawler = $client->click($crawler->selectLink('Add new contact')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Save')->form(array(
            'appbundle_contact[firstName]'  => 'A',
            'appbundle_contact[lastName]'  => 'B',
            'appbundle_contact[streetAndNumber]'  => 'Test 21',
            'appbundle_contact[zipCode]'  => '11111',
            'appbundle_contact[city]'  => 'TestCity',
            'appbundle_contact[country]'  => 'TestCountry',
            'appbundle_contact[phoneNumber]'  => '902222222',
            'appbundle_contact[emailAddress]'  => 'emailAddress',
            'appbundle_contact[birthMonth]'  => 'January',
            'appbundle_contact[birthDay]'  => '21',
        ));

        $crawler = $client->submit($form);
        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('li:contains("Your first name must be at least 2 characters long")')->count(), 'Missing element li:contains("Your first name must be at least 2 characters long")');
        $this->assertGreaterThan(0, $crawler->filter('li:contains("Your last name must be at least 2 characters long")')->count(), 'Missing element li:contains("Your last name must be at least 2 characters long")');
        $this->assertGreaterThan(0, $crawler->filter('li:contains("The email ""emailAddress"" is not a valid email.")')->count(), 'Missing element li:contains("The email ""emailAddress"" is not a valid email.")');
    }

    public function test404Scenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact/qwer');
        $this->assertEquals(404, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /contact/");
    }

    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = static::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/contact/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode(), "Unexpected HTTP status code for GET /contact/");
        $crawler = $client->click($crawler->selectLink('Add new contact')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Save')->form(array(
            'appbundle_contact[firstName]'  => 'TestFirstName',
            'appbundle_contact[lastName]'  => 'TestLastName',
            'appbundle_contact[streetAndNumber]'  => 'Test 21',
            'appbundle_contact[zipCode]'  => '11111',
            'appbundle_contact[city]'  => 'TestCity',
            'appbundle_contact[country]'  => 'TestCountry',
            'appbundle_contact[phoneNumber]'  => '902222222',
            'appbundle_contact[emailAddress]'  => 'emailAddress@example.com',
            'appbundle_contact[birthMonth]'  => 'January',
            'appbundle_contact[birthDay]'  => '21',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Save')->form(array(
            'appbundle_contact[firstName]'  => 'EditedFirstName',
            'appbundle_contact[lastName]'  => 'EditedLastName',
            'appbundle_contact[streetAndNumber]'  => 'Edited 21',
            'appbundle_contact[zipCode]'  => '11111',
            'appbundle_contact[city]'  => 'TestCity',
            'appbundle_contact[country]'  => 'TestCountry',
            'appbundle_contact[phoneNumber]'  => '902222222',
            'appbundle_contact[emailAddress]'  => 'emailAddress@example.com',
            'appbundle_contact[birthMonth]'  => 'January',
            'appbundle_contact[birthDay]'  => '21',
        ));

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view (look for td with value equals "EditedFirstName")
        $this->assertGreaterThan(0, $crawler->filter('td:contains("EditedFirstName")')->count(), 'Missing element td:contains("EditedFirstName")');

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        $this->assertNotRegExp('/EditedFirstName/', $client->getResponse()->getContent());
    }


}
