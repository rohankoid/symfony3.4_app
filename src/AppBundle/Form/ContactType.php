<?php

namespace AppBundle\Form;

use AppBundle\Repository\ContactRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('lastName', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('streetAndNumber', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('zipCode', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('city', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('country', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('phoneNumber', TextType::class, array('attr' => array('class' => 'form-control form-group')))
            ->add('birthMonth', ChoiceType::class, ['choices' => ContactRepository::MONTHS, 'attr' => array('class' => 'form-control form-group')])
            ->add('birthDay', IntegerType::class, array('attr' => array('class'=> 'form-control form-group')))
            ->add('emailAddress', TextType::class, array('attr' => array('class' => 'form-control form-group')));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Contact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_contact';
    }


}
