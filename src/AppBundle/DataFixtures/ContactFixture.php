<?php

namespace AppBundle\DataFixtures;
use AppBundle\Entity\Contact;
use AppBundle\Repository\ContactRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class ContactFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $arrayOfCities = array('Frankfurt', 'Darmstadt', 'Stuttgart', 'Berlin');
        for ($i = 0; $i < 10; $i++) {
            $randomLength = rand(1, 10);
            $length = 10;
            $contact = new Contact();
            $contact->setFirstName(substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length).$i);
            $contact->setLastName(substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length).$i);
            $contact->setCity($arrayOfCities[array_rand($arrayOfCities)]);
            $contact->setCountry("Germany");
            $contact->setZipCode(mt_rand(1111, 99999));
            $contact->setBirthDay(mt_rand(1, 29));
            $contact->setBirthMonth(array_rand(ContactRepository::MONTHS));
            $contact->setPhoneNumber(rand(1111111111,9999999999));
            $contact->setEmailAddress(substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length). '@email.com');
            $contact->setStreetAndNumber(substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,$length). ' '. $randomLength);
            $manager->persist($contact);
        }

        $manager->flush();
    }
}