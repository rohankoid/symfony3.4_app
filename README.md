## Simple Web application in symfony 3.4.

#### Technology used:
    - Symfony 3.4
    - Doctrine with SQLite
    - Twig
    - PHP 7.0

### Installation

#### Pre-requisites

- PHP 7.0 installed
- Composer installed
- SQLite server installed 

> Make sure var/data directory exists and have write access on it.

On project root directory:

#### Install dependencies

`$ composer install`

#### Configuration

Make a copy of `parameters.yml.dist` as `parameters.yml`.

#### Create database

`$ php bin/console doctrine:database:create`

This creates database file (database_path) as configured in parameters.yml

> Make sure var/data/data.sqlite (or database_path defined in parameters.yml) is writeable by server user.

#### Create tables (migration)

To create table and run any migration stored in app/DoctrineMigrations:

``php bin/console doctrine:migrations:migrate``

To run any specific migration or learn more on this: [Doctrine Migrations](https://symfony.com/doc/1.3/bundles/DoctrineMigrationsBundle/index.html)

#### (Optional) Seed the database
If you would like to start the application with some fixtures:

 `$ php bin/console doctrine:fixtures:load`

To run any specific Fixture or learn more on this: [Doctrine Fixtures](https://symfony.com/doc/master/bundles/DoctrineFixturesBundle/index.html)

### Run functional tests
`$ ./vendor/bin/simple-phpunit src/AppBundle/Tests/Controller/ContactControllerTest.php`

### Deploying
To deploy please go through this [installation guide](https://symfony.com/doc/current/setup/symfony_server.html)

##### Notes
> This version of the application was setup using "Symfony Standard Edition".

> Standard bootstrap has been used for css and js. 
